#!/bin/bash

path=$(realpath $0)
path=$(dirname $path)
rm -f $path/nextcloud.sql.1
[ -f $path/nextcloud.sql ] && mv -f $path/nextcloud.sql $path/nextcloud.sql.1
mysqldump --defaults-extra-file=$path/my.cnf -u root nextcloud > $path/nextcloud.sql
