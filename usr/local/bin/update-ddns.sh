#!/bin/bash

# https://api.dynu.com/nic/update?username=USERNAME&password=PASSWORD
OUTPUT=$(echo url="___DDNS_UPDATE_URL___" | curl -s -K -)
logger "Update DDNS : $OUTPUT"
