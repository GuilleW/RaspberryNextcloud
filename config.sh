#!/bin/bash

if [ "$(id -u)" -ne 0 ]; then
  echo "$0 is not running as root. Try using sudo."
  exit 1
fi

mkdir -p tmp
touch tmp/env.txt
ROOT="$(dirname "$(realpath $0)")"

check_binary_available() {
  if ! type $1 &> /dev/null; then
  echo "ERROR : Fail to install $1. Exit !!!"
  exit 0
  fi
  echo -e "\nPackage $1 installed! Continue in 3s ..."; sleep 3
}

# ==========================================
# Backup nextcloud data
# ==========================================
mysql_path=""
nc_backup_sql=""
nc_data_path=""
nc_apps_path=""
backup_size=0
declare -a available_drives

incorrect_selection() {
  echo "Incorrect selection! Try again."
}

cp_progress() {
  rsync -a --info=progress2 --no-i-r "$1" "$2"
}

check_mounted_nextcloud() {

  echo ""
  echo "Search your Nextcloud instance..."
  declare -a arr=(`df -h --output=target`)
  for k in "${arr[@]}"; do
    if [ -d "$k/home/pi/nextcloud-data" ] || [ -d "$k/home/pi/nextcloud-data-orig" ]; then
      nc_data_path="$k/home/pi/nextcloud-data"
      if [ -d "$k/var/lib/mysql" ] || [ -d "$k/var/lib/mysql-orig" ]; then
        nc_backup_sql="$k/home/pi/nextcloud-db"
        mysql_path="$k/var/lib/mysql"
      fi
      if [ -d "$k/var/www/nextcloud/apps" ] || [ -d "$k/var/www/nextcloud/apps-orig" ]; then
        nc_apps_path="$k/var/www/nextcloud/apps"
      fi
      break
    fi
  done

  if [[ "$mysql_path" = "" || "$nc_data_path" = "" || "$nc_apps_path" = "" ]]; then
    echo "Error: Nextcloud instance not found. Is device mounted?"
    return 1
  else
    return 0
  fi

}

not_same_device() {
  device1="$(df "$1" | tail -1 | awk '{ gsub(/[0-9]+/,""); print $1; }')"
  device2="$(df "$2" | tail -1 | awk '{ gsub(/[0-9]+/,""); print $1; }')"
  if [ "$device1" = "$device2" ]; then
    return 1
  fi
  return 0
}

list_available_drives_to_store_backup() {
  echo ""
  echo "*****************************************"
  echo ""
  echo "Select a place to store backup..."
  echo ""
  mapfile -t lines < <(df -h --output=avail,target | sort -rh)
  available_drives=("")
  i=0
  for line in "${lines[@]}"; do
    size="$(echo $line | awk '{print $1}')"
    size="$(numfmt --from=iec --invalid='ignore' $size)"
    path="$(echo $line | awk '{print $2}')"

    # Enough space
    if (( size > backup_size )); then
      # don't backup on same device
      if not_same_device "$nc_data_path" "$path"; then
        # let's go
        i=$(($i+1))
        if (($i<10)); then
          echo "   $i  -  $line"
        else
          echo "  $i  -  $line"
        fi
        available_drives+=("$path")
      fi
    fi
  done

}

backup_nextcloud() {

  # save nextcloud version to install same version on install process
  nextcloud_version="$(cat $nc_apps_path/../config/config.php | grep 'version' | awk -F\' '{print $4}')"
  nextcloud_version="$(echo $nextcloud_version | sed 's/\.[0-9]*$//g')"
  echo "nextcloud_version=$nextcloud_version" >> "tmp/env.txt"

  d=`date +%Y-%m-%d-%H-%M-%S`
  backup_path="${available_drives[$select_path]}/raspberry-nextcloud-backup/$d"
  mkdir -p "$backup_path"

  echo "Backup Nextcloud (v$nextcloud_version) to ${backup_path} :"
  echo "  - Backup Mysql ..."
  cp_progress "$nc_backup_sql" "$backup_path"
  cp_progress "$mysql_path" "$backup_path"
  echo "  - Backup Nextcloud data ..."
  cp_progress "$nc_data_path" "$backup_path"
  echo "  - Backup Nextcloud apps ..."
  cp_progress "$nc_apps_path" "$backup_path"

}

user_selection_backup_nextcloud(){

  echo ""
  echo "*****************************************"
  echo ""

  if ! check_mounted_nextcloud; then
    return 1
  fi

  backup_size=$(du -c \
    $nc_backup_sql \
    $mysql_path \
    $nc_data_path \
    $nc_apps_path \
    | grep total | awk '{print $1}')

  echo "Nextcloud data found on your filesystem."
  echo "We'll backup this folders ($(numfmt --to=iec $backup_size)):"
  echo "  - mysql : $mysql_path"
  echo "  - Nextcloud data : $nc_data_path"
  echo "  - Nextcloud apps : $nc_apps_path"
  echo ""
  list_available_drives_to_store_backup
  echo "   0  -  Exit"
  echo ""
  unset select_path
  until [ "$select_path" = "0" ]; do
    read -p "Enter selection: " select_path
    echo ""
    if [ "$select_path" = "0" ]; then
      return
    fi
    if (( select_path > 0 && select_path < ${#available_drives[@]} )); then
      backup_nextcloud
      select_path=0
    else
      incorrect_selection
      select_path=-1
    fi
  done

}


# ==========================================
# Configure your Raspberry storage
# ==========================================
check_mounted_sdcard() {

  partition_boot=$(cat /proc/mounts | grep 'boot')
  partition_rootfs=$(cat /proc/mounts | grep 'rootfs')

  device_boot=$(echo $partition_boot | awk '{print $1}' | sed 's/[0-9]*//g' )
  device_rootfs=$(echo $partition_rootfs | awk '{print $1}' | sed 's/[0-9]*//g' )

  path_boot=$(echo $partition_boot | awk '{print $2}' )
  path_rootfs=$(echo $partition_rootfs | awk '{print $2}' )

  if [ "$device_boot" != "$device_rootfs" ]; then
    echo "Device for boot and rootfs should be the same:"
    echo "  - boot: $device_boot"
    echo "  - rootfs: $device_rootfs"
    echo ""
    echo "Unmount other partitions with boot or rootfs name, and run me again."
    return 1
  fi;

  if [ "$path_boot" = "" ] || [ "$path_rootfs" = "" ]; then
    echo "Raspberry partitions not found:"
    echo "  - boot: $path_boot"
    echo "  - rootfs: $path_rootfs"
    echo ""
    echo "Can't found Raspberry storage, be sure to mount it, and run me again."
    return 1
  fi;

  return 0
}

configure_var() {

  declare -A responses
  source tmp/env.txt

  prompt() {

    if [ "${!2}" = "" ]; then
      read -p "$1: " str
    else
      read -p "$1 [${!2}]: " str
    fi
    responses[$2]=${str:-${!2}}
  }
  prompt "Your WIFI SSID" "wifi_id"
  prompt "Your WIFI Pass" "wifi_pass"
  prompt "Your WiFi country code (e.g. US)" "country_code"
  prompt "Enable 64-bit raspbian kernel (y/N)" "mode64"
  prompt "Static IP of your Raspberry for DHCP" "static_ip"
  prompt "Domain to access your Raspberry from internet" "domain"
  prompt "Working URL of your DDNS service" "ddns_update_url"
  prompt "Nextcloud port (e.g. 8080)" "nextcloud_port"
  prompt "Configure your MariaDB root password" "mysql_root_password"
  prompt "Configure your Nextcloud database password" "mysql_nextcloud_password"
  prompt "Your email (so server can mail you)" "mail"
  prompt "Your email password (for SMTP relay)" "mail_pass"
  prompt "Your Nextcloud account name" "nextcloud_user"
  prompt "Your Nextcloud account password" "nextcloud_pass"
  if [ "$mode64" != "y" ]; then
    mode64="n"
  fi
  
  # Set @ as delimiter
  IFS='@'
  read -a strarr <<< "$mail"
  responses[mail_user]=${strarr[0]}
  responses[mail_domain]=${strarr[1]}

  echo "" > tmp/env.txt
  echo "nextcloud_version=$nextcloud_version" >> tmp/env.txt
  for key in "${!responses[@]}"
    do echo "$key=${responses[$key]@Q}" >> tmp/env.txt
  done
  source tmp/env.txt

}

update_config() {

  echo "Copy packages config..."
  cd tmp
  ls -Q | grep -v env.txt | xargs rm -fr
  cd ..

  cp_progress "./boot" "./tmp"
  cp_progress "./etc" "./tmp"
  cp_progress "./home" "./tmp"
  cp_progress "./var" "./tmp"

  echo "Configuring packages config..."
  find tmp -type f -exec sed -i -e 's/___MAIL___/'$mail'/g' {} \;
  find tmp -type f -exec sed -i -e 's/___MAIL_USER___/'$mail_user'/g' {} \;
  find tmp -type f -exec sed -i -e 's/___MAIL_PASS___/'$mail_pass'/g' {} \;
  find tmp -type f -exec sed -i -e 's/___MAIL_DOMAIN___/'$mail_domain'/g' {} \;
  find tmp -type f -exec sed -i -e 's/___COUNTRY_CODE_2_LETTERS___/'$country_code'/g' {} \;
  find tmp -type f -exec sed -i -e 's/___WIFI_ID___/'$wifi_id'/g' {} \;
  find tmp -type f -exec sed -i -e 's/___WIFI_PASS___/'$wifi_pass'/g' {} \;
  find tmp -type f -exec sed -i -e 's/___STATIC_IP___/'$static_ip'/g' {} \;
  find tmp -type f -exec sed -i -e 's/___DOMAIN___/'$domain'/g' {} \;
  find tmp -type f -exec sed -i -e 's/___DDNS_UPDATE_URL___/'$ddns_update_url'/g' {} \;
  find tmp -type f -exec sed -i -e 's/___NEXTCLOUD_PORT___/'$nextcloud_port'/g' {} \;
  find tmp -type f -exec sed -i -e 's/___MYSQL_ROOT_PASSWORD___/'$mysql_root_password'/g' {} \;

  echo "Generate your Nginx certificate..."
  echo ""
  openssl dhparam -out tmp/etc/nginx/snippets/dh_param.pem 4096

  echo ""
  echo "Enable SSH on your headless Raspberry"
  touch $path_boot/ssh

  if [ "$mode64" = "y" ]; then
    echo "Enable 64-bit raspbian kernel..."
    if [ "$(cat $path_boot/config.txt | grep '#RaspberryNextcloud')" = "" ]; then
      cat tmp/boot/config.txt >> $path_boot/config.txt
    fi;
  fi;

  echo "Enable and configure your Wifi..."
  cat tmp/boot/wpa_supplicant.conf > $path_boot/wpa_supplicant.conf

  echo "Copy static IP configuration to storage on rootfs partition..."
  if [ "$(cat $path_rootfs/etc/dhcpcd.conf | grep '#RaspberryNextcloud')" = "" ]; then
    cat tmp/etc/dhcpcd.conf.part >> $path_rootfs/etc/dhcpcd.conf
  fi;

  echo "Copy your settings on storage..."
  path_rpi=$path_rootfs/home/pi/$(basename "$PWD")
  rm -fr $path_rpi
  cp_progress "../$(basename $PWD)" "$path_rootfs/home/pi"
  chown -R root:root  $path_rpi
  chmod -R u=rwX,go=r $path_rpi
  chmod -R a+X        $path_rpi

  echo ""
  echo "Your storage is ready!"
  echo "Now unmount your storage, and start your Raspberry Pi with it!"
  sleep 1

}

user_selection_copy_config_on_sdcard(){

  if ! check_mounted_sdcard; then
    sleep 1
    return 1
  fi

  configure_var

  unset go_update_config
  until [ "$go_update_config" = "y" ]; do
    echo ""
    read -p "  Confirm ? (y/N) " go_update_config
    echo ""
    case $go_update_config in
      y ) update_config ;;
      * ) return ;;
    esac
  done

}


# ==========================================
# Boot Raspberry from SSD
# ==========================================
user_selection_boot_from_ssd() {
  echo ""
  sd="$(lsblk -d | awk '/sd/{print $1}')"
  len="$(echo "$sd" | awk 'END{ print NR }')"

  if [ "$len" -eq "0" ]; then
    echo "Error: No USB Storage found."
    return 1
  elif [ "$len" -gt "1" ]; then
    echo "Error: Too many USB Storage found."
    echo "       Please, insert only one for this action."
    return 1
  fi

  echo "Copy SD card on USB storage..."
  dd if="/dev/mmcblk0" of="/dev/$sd" status=progress
  echo "Extend file system to fit max storage..."
  e2fsck -fp "/dev/${sd}2"
  parted "/dev/${sd}" resizepart 2 100%
  resize2fs "/dev/${sd}2"
  echo "You can now remove SD card, and restart on USB storage."
}


# ==========================================
# Install nextcloud
# ==========================================
we_are_on_rpi() {
  os="$(cat /etc/os-release | grep 'Raspbian')"
  if [ "$os" = "" ]; then
    return 1
  fi
  return 0
}

user_selection_install_nextcloud() {

  if ! we_are_on_rpi; then
    echo "To protect your system, you can't start install if you're not on a Raspbian."
    sleep 1
    return 1
  fi

  touch tmp/env.txt
  source tmp/env.txt

  chown -R root:root  tmp
  chmod -R u=rwX,go=r tmp
  chmod -R a+X        tmp

  echo ""
  echo "***********"
  echo "Update OS:"
  echo "***********"
  apt update
  apt full-upgrade -y
  apt autoremove -y
  echo -e "\nPackage installed! Continue in 3s ..."; sleep 3


  echo ""
  echo "****************"
  echo "Install Postfix:"
  echo "****************"
  debconf-set-selections <<< "postfix postfix/mailname string $hostname"
  debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
  apt install -y postfix
  cp_progress "tmp/etc/postfix" "/etc"
  cp_progress "tmp/etc/aliases" "/etc"
  newaliases
  postmap /etc/postfix/virtual
  chown root:root /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db
  chmod -R u=rw,go= /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db
  service postfix restart
  check_binary_available postfix


  echo ""
  echo "****************"
  echo "Install MariaDB:"
  echo "****************"
  apt install -y mariadb-server
  cp_progress "tmp/etc/mysql" "/etc"
  echo "Stop MariaDB service..."
  service mysql stop
  echo -n "Start MariaDB in safe mode..."
  mysqld_safe --skip-grant-tables --skip-networking &
  while ! mysqladmin ping --silent; do
    echo -n "."
    sleep 1
  done
  echo "Update root password..."
  mysql -e "FLUSH PRIVILEGES; ALTER USER 'root'@'localhost' IDENTIFIED BY '$mysql_root_password';"
  echo "Exit safe mode..."
  mysqladmin -p$mysql_root_password shutdown
  echo "Start MariaDB service..."
  service mysql start
  check_binary_available mysql


  echo ""
  echo "**************"
  echo "Install PHP 7:"
  echo "**************"
  apt install -o Dpkg::Options::="--force-confnew" -y \
    php-apcu \
    php7.4-bcmath \
    php7.4-cli \
    php7.4-common \
    php7.4-curl \
    php7.4-fpm \
    php7.4-gd \
    php7.4-gmp \
    php-igbinary \
    php-imagick \
        imagemagick \
    php7.4-imap \
    php7.4-intl \
    php7.4-json \
    php7.4-mbstring \
    php7.4-mysql \
    php7.4-opcache \
    php7.4-readline \
    php7.4-sqlite3 \
    php7.4-xml \
    php7.4-zip
  cp_progress "tmp/etc/php" "/etc"
  service php7.4-fpm reload
  check_binary_available php


  echo ""
  echo "**************"
  echo "Install Nginx:"
  echo "**************"
  apt remove apache2
  apt install -y nginx libnginx-mod-http-headers-more-filter
  proc=$(grep processor /proc/cpuinfo | wc -l)
  sed -i -e 's/___NGINX_WORKER_PROCESSES___/'$proc'/g' tmp/etc/nginx/nginx.conf
  cp_progress "tmp/etc/nginx" "/etc"
  cp_progress "tmp/var/www" "/var"
  ln -sfn /etc/nginx/sites-available/default.selfsigned /etc/nginx/sites-enabled/default
  service nginx reload
  check_binary_available nginx


  echo ""
  echo "*****************************"
  echo "Install Let's Encrypt Client:"
  echo "*****************************"
  echo "Install Let's Encrypt as certificates auto deployment bot."
  apt install -y certbot
  cp -v tmp/etc/cron.daily/certbot-reload-nginx /etc/cron.daily
  chmod a+x /etc/cron.daily/certbot-reload-nginx
  echo "Ask our first valid certificate from Let's Encrypt and use it in our default config."
  certbot certonly --non-interactive --agree-tos --email "$mail" --webroot -w /var/www/html/ -d $domain
  ln -sfn /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default
  service nginx reload
  check_binary_available certbot


  echo ""
  echo "******************"
  echo "Install Nextcloud:"
  echo "******************"
  echo ""
  if [ "$nextcloud_version" != "" ]; then
    echo "Download Nextcloud (v$nextcloud_version):"
    v="nextcloud-$nextcloud_version"
  else
    echo "Download latest Nextcloud:"
    v="latest"
  fi
  link="https://download.nextcloud.com/server/releases/$v.tar.bz2"
  wget -c "$link" -O "/tmp/nextcloud-$v.tar.bz2"
  if ! [ -f "/tmp/nextcloud-$v.tar.bz2" ]; then
    echo "Error: Nextcloud version $v not downloaded."
    return 1
  fi
  echo "Extracting to /var/www/nextcloud ..."
  tar xvpf "/tmp/nextcloud-$v.tar.bz2" -C "/var/www" 2>&1 |
  while read line; do
    x=$((x+1))
    echo -en "$x files extracted...\r"
  done
  chown -R www-data:www-data /var/www/nextcloud
  chmod -R u=rwX,go= /var/www/nextcloud
  mkdir -p /home/pi/nextcloud-data
  chown -R www-data:www-data /home/pi/nextcloud-data
  chmod -R u=rwX,go= /home/pi/nextcloud-data
  echo ""
  echo "Create Nextcloud database..."
  mysql -u root -p$mysql_root_password -e " \
    CREATE USER IF NOT EXISTS 'nextcloud'@'localhost' IDENTIFIED BY '$mysql_nextcloud_password'; \
    CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci; \
    GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'localhost' IDENTIFIED BY '$mysql_nextcloud_password'; \
    FLUSH privileges;"
  echo "Enable nextcloud site and reload nginx..."
  ln -fs /etc/nginx/sites-available/nextcloud /etc/nginx/sites-enabled/nextcloud
  service nginx reload
  echo "Create Nextcloud cron job..."
  echo "*/15  *  *  *  * php -f /var/www/nextcloud/cron.php" | crontab -u www-data -
  echo "Install Nextcloud..."
  occ() {
    sudo -u www-data php /var/www/nextcloud/occ "$@"
  }
  occ maintenance:install \
    --database "mysql" \
    --database-name "nextcloud" \
    --database-user "nextcloud" \
    --database-pass "$mysql_nextcloud_password" \
    --admin-user "$nextcloud_user" \
    --admin-pass "$nextcloud_pass" \
    --data-dir "/home/pi/nextcloud-data"
  occ config:system:set \
    trusted_domains 2 --value="$domain"
  occ config:system:set \
    'memcache.local' --value='\OC\Memcache\APCu'
  occ background:cron
  occ config:system:set default_phone_region --type string --value="FR"
  echo -e "\nPackage installed! Continue in 3s ..."; sleep 3


  echo ""
  echo "******************"
  echo "Install Custom Backup Database:"
  echo "******************"
  echo ""
  echo "Create a ~/nextcloud-db/ folder and backup every night the database."
  echo "This backup is used by this script to restore Nextcloud in case of data loss."
  cp_progress "tmp/home/pi/nextcloud-db" "/home/pi"
  path=/home/pi/nextcloud-db
  chown -R root:root $path
  chmod -R u=rwX,go= $path
  chmod -R a+X       $path
  chmod a+x          $path/mysqldump.sh
  echo "Create Backup cron job..."
  echo "0 3 * * * $path/mysqldump.sh" | crontab -u root -
  echo -e "\nScript installed! Continue in 3s ..."; sleep 3

  echo ""
  echo "******************"
  echo "Install DDNS service:"
  echo "******************"
  echo ""
  echo "This script run every 5 minutes to keep your hostname updated to the most current IP address using cron job. "
  cp_progress "tmp/usr/local/bin/update-ddns.sh" "/usr/local/bin/update-ddns.sh"
  path=/usr/local/bin/update-ddns.sh
  chown -R root:root $path
  chmod u=rwx $path
  chmod go=x $path
  echo "Create cron job..."
  echo "*/5 * * * * $path" | crontab -u root -
  echo -e "\nScript installed! Continue in 3s ..."; sleep 3

  echo ""
  echo "*************"
  echo "Install done!"
  echo "*************"
  echo ""
  echo ""
  echo "Now go to your https://$domain:$nextcloud_port and enjoy your awesome cloud server."
  echo "Don't forget to run uninstall from your computer !"
  echo "The script contains your passwords in plain text!!!"
  echo ""
  echo ""
}


# ==========================================
# Restore nextcloud data
# ==========================================
declare -a backups
declare -a available_backups

get_backups() {
  backups=()
  mapfile -t parents < <(df --output=target)
  for parent in "${parents[@]}"; do
    if [ -d "$parent/raspberry-nextcloud-backup" ]; then
      path="$parent/raspberry-nextcloud-backup"
      for timestamp in $(ls "$path"); do
        backups+=("$timestamp ($parent)")
      done
    fi
  done
  readarray -t sorted < <(printf '%s\n' "${backups[@]}" | sort -r)
  backups=("${sorted[@]}")
}

list_available_backups_to_restore () {
  get_backups
  available_backups=("")
  i=0
  for backup in "${backups[@]}"; do
    i=$(($i+1))
    if (($i<10)); then
      echo "   $i  -  $backup"
    else
      echo "  $i  -  $backup"
    fi
    available_backups+=("$backup")
  done
}

restore_nextcloud() {

  echo "Backup original packages folders..."
  if [ -d "$mysql_path" ] && ! [ -d "$mysql_path-orig" ]; then
    echo "$mysql_path -> $mysql_path-orig"
    mv "$mysql_path" "$mysql_path-orig"
  fi
  if [ -d "$nc_data_path" ] && ! [ -d "$nc_data_path-orig" ]; then
    echo "$nc_data_path -> $nc_data_path-orig"
    mv "$nc_data_path" "$nc_data_path-orig"
  fi
  if [ -d "$nc_apps_path" ] && ! [ -d "$nc_apps_path-orig" ]; then
    echo "$nc_apps_path -> $nc_apps_path-orig"
    mv "$nc_apps_path" "$nc_apps_path-orig"
  fi

  restore_path="${available_backups[$select_backup]}"
  restore_path=$(echo "$restore_path" | awk -F"[() ]" '{printf $3; printf "/raspberry-nextcloud-backup/"; printf $1}')
  if [ -d "$restore_path" ]; then
    echo "Restore from ${available_backups[$select_backup]} :"

    echo "  - Restore Mysql ..."
    rm -fr "$mysql_path"
    cp_progress "$restore_path/mysql" "$(dirname $mysql_path)"
    IFS=" " read uid gid <<< "$(ls -nd "$mysql_path-orig" | awk '{print $3,$4}')";
    chown -R $uid:$gid "$mysql_path"
    chmod -R ugo=rwX "$mysql_path"

    echo "  - Restore Nextcloud data ..."
    rm -fr "$nc_data_path"
    cp_progress "$restore_path/nextcloud-data" "$(dirname $nc_data_path)"
    IFS=" " read uid gid <<< "$(ls -nd "$nc_data_path-orig" | awk '{print $3,$4}')";
    chown -R $uid:$gid "$nc_data_path"
    chmod -R u=rwX,go= "$nc_data_path"

    echo "  - Restore Nextcloud apps ..."
    rm -fr "$nc_apps_path"
    cp_progress "$restore_path/apps" "$(dirname $nc_apps_path)"
    IFS=" " read uid gid <<< "$(ls -nd "$nc_apps_path-orig" | awk '{print $3,$4}')";
    chown -R $uid:$gid "$nc_apps_path"
    chmod -R u=rwX,go= "$nc_apps_path"
  fi

}

user_selection_restore_nextcloud() {

  echo ""
  echo "*****************************************"
  echo ""

  if ! check_mounted_nextcloud; then
    return 1
  fi

  echo ""
  echo "Select a backup to restore..."
  echo ""

  list_available_backups_to_restore
  echo "   0  -  Exit"
  echo ""
  unset select_backup
  until [ "$select_backup" = "0" ]; do
    read -p "Enter selection: " select_backup
    echo ""
    if [ "$select_backup" = "0" ]; then
      return
    fi
    if (( select_backup > 0 && select_backup < ${#available_backups[@]} )); then
      restore_nextcloud
      select_backup=0
    else
      incorrect_selection
      select_backup=-1
    fi
  done

}


# ==========================================
# Uninstall RaspberryNextcloud (and backups)
# ==========================================
user_selection_uninstall() {

  read -p "  Continue ? (y/N) " continue
  if [ ! "$continue" = "y" ]; then
    return 1
  fi

  echo ""
  echo "****************"
  echo "Clean up install"
  echo "****************"
  echo ""

  echo "Remove backups..."
  get_backups
  for backup in "${backups[@]}"; do
    restore_path=$(echo "$backup" | awk -F"[() ]" '{printf $3; printf "/raspberry-nextcloud-backup"}')
    echo "  - $restore_path"
    rm -fr "$backup"
  done

  echo "Remove RaspberryNextcloud on rpi..."
  ssh \
    -o StrictHostKeyChecking=no \
    -o UserKnownHostsFile=/dev/null \
    pi@raspberrypi.local \
    'sudo rm -fr /home/pi/nextcloud-data-orig;' \
    'sudo rm -fr /var/lib/mysql-orig;' \
    'sudo rm -fr /var/www/nextcloud/apps-orig;'

  echo "Remove RaspberryNextcloud installer..."
  read -p "  Continue ? (y/N) " continue
  if [ "$continue" = "y" ]; then
    echo "  - $ROOT ..."
    rm -fr "$ROOT"
  fi

  sleep 3

}


# ==========================================
# User prompt
# ==========================================
menu_on_user_system() {
  until [ "$selection" = "0" ]; do
    echo ""
    echo "*****************************************"
    echo ""
    echo "  1  -  Backup Nextcloud data (optional)"
    echo "  2  -  Configure your Raspberry storage"
    echo "  3  -  Restore Nextcloud data (optional)"
    echo "  4  -  Uninstall RaspberryNextcloud (and backups)"
    echo "  0  -  Exit"
    echo ""
    read -p "Enter selection: " selection
    echo ""
    case $selection in
      1 ) user_selection_backup_nextcloud ;;
      2 ) user_selection_copy_config_on_sdcard ;;
      3 ) user_selection_restore_nextcloud ;;
      4 ) user_selection_uninstall ;;
      0 ) exit ;;
      * ) incorrect_selection ;;
    esac
  done
}

menu_on_rpi() {
  until [ "$selection" = "0" ]; do
    echo ""
    echo "*****************************************"
    echo ""
    echo "  1  -  Boot Raspberry from SSD (optional but recommanded)"
    echo "  2  -  Install your Nextcloud server"
    echo "  0  -  Exit"
    echo ""
    read -p "Enter selection: " selection
    echo ""
    case $selection in
      1 ) user_selection_boot_from_ssd ;;
      2 ) user_selection_install_nextcloud ;;
      0 ) exit ;;
      * ) incorrect_selection ;;
    esac
  done
}

if we_are_on_rpi; then
  menu_on_rpi
else
  menu_on_user_system
fi
