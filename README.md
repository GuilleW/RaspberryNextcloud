# 🪧 MyRaspberry

Configuration used for my Raspberry Pi (and a Nextcloud server).
If you broke your storage you can use the
[Backup](#-backup-nextcloud-data-optional) and
[Restore](#-restore-nextcloud-data-optional) feature to backup your data if possible.


## Summary
<!-- TOC START min:1 max:3 link:true asterisk:false update:true -->
- [🪧 MyRaspberry](#-myraspberry)
	- [Summary](#summary)
- [🖥️ On your computer](#-on-your-computer)
	- [📥 Download MyRaspberry](#-download-myraspberry)
	- [🚨 Backup Nextcloud data (optional)](#-backup-nextcloud-data-optional)
	- [⚡ Install Raspberry Pi OS](#-install-raspberry-pi-os)
	- [🔧 Configure your Raspberry storage](#-configure-your-raspberry-storage)
- [⚡ On your raspberry](#-on-your-raspberry)
	- [🔌 Connect your Raspbery Pi](#-connect-your-raspbery-pi)
	- [🚀 Boot Raspberry From SSD (optional but recommanded)](#-boot-raspberry-from-ssd-optional-but-recommanded)
		- [Update Firmware](#update-firmware)
		- [Boot Raspberry from USB](#boot-raspberry-from-usb)
	- [📦 Install Web Server Packages](#-install-web-server-packages)
- [🖥️ On your computer](#-on-your-computer-1)
	- [🚨 Restore Nextcloud data (optional)](#-restore-nextcloud-data-optional)
<!-- TOC END -->


# 🖥️ On your computer

## 📥 Download MyRaspberry

Download from git (or zip file).

```bash
git clone https://gitlab.com/GuilleW/RaspberryNextcloud.git
cd RaspberryNextcloud
```


## 🚨 Backup Nextcloud data (optional)

Connect your storage to your computer and run the script to backup Nextcloud data.

```bash
sudo ./config.sh
```

Select `1  -  Backup Nextcloud data (optional)`.

Select `0  -  Exit"`.


## ⚡ Install Raspberry Pi OS

Install Raspberry Pi OS using [Raspberry Pi Imager](https://www.raspberrypi.org/software/).

1. Choose OS
	- Raspberry Pi OS (other)
		- Raspberry Pi OS Lite (32-bit)
1. Storage
	-	 *Select your storage*
1. Write
	- Yes


## 🔧 Configure your Raspberry storage

Run the script to configure your headless Raspbery Pi.

```bash
sudo ./config.sh
```

Select `2  -  Configure your Raspberry storage`.

Select `0  -  Exit"`.

Now your Raspberry is ready with your data.


# ⚡ On your raspberry

## 🔌 Connect your Raspbery Pi

You can now unmount your storage and plug it to the Raspberry Pi.
Continue through SSH.

```bash
ssh pi@raspberrypi.local
// remember default pass is -> raspberry
```

Tune your Raspberry:
```bash
sudo raspi-config
```

If you use a SSD as storage or don't want save your SD card jump to [Install Web Server Packages](#-install-web-server-packages).


## 🚀 Boot Raspberry From SSD (optional but recommanded)


### Update Firmware

```bash
sudo apt update
sudo rpi-update
sudo reboot
```

After reboot, login again and install the latest bootloader.

```bash
sudo rpi-eeprom-update -d -a
```

After reboot login again and select latest version boot ROM software:

```bash
sudo raspi-config
```

- Advanced Options
	- Bootloader version
	- Latest Use the latest version boot ROM software
	- Reset boot ROM to defaults?
	 	- No
	- OK

⚠️ Don't reboot now.

### Boot Raspberry from USB

```bash
sudo raspi-config
```

- Advanced Options
	- Boot Order
	- USB Boot
	- OK

```bash
cd RaspberryNextcloud
sudo ./config.sh
```

Select `1  -  Boot Raspberry from SSD (optional but recommanded)"`.

Select `0  -  Exit"`.

Shutdown the Raspbery Pi, leave only the SSD drive connected and throw away your SD card!

```bash
sudo halt
```

## 📦 Install Web Server Packages

Continue through SSH.

```bash
ssh pi@raspberrypi.local
// remember default pass is -> raspberry

cd RaspberryNextcloud
sudo ./config.sh
```

Select `2  -  Install your Nextcloud server`.

**You're done unless you need to restore your Nextcloud data!**


# 🖥️ On your computer

## 🚨 Restore Nextcloud data (optional)

```bash
sudo halt
```

Connect your storage to your computer and run the script to restore Nextcloud data.

```bash
cd RaspberryNextcloud
sudo ./config.sh
```

Select `3  -  Restore Nextcloud data (optional)`.

Select `0  -  Exit"`.

You can now unmount your storage and plug it to the Raspberry Pi.
